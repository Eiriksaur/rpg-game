package main.java.consolehelpers;

import main.java.characters.Hero;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melees.Paladin;
import main.java.characters.melees.Rogue;
import main.java.characters.melees.Warrior;
import main.java.characters.ranged.Ranger;
import main.java.characters.supports.Druid;
import main.java.characters.supports.Priest;
import main.java.factories.*;
import main.java.items.armor.*;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.*;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;

import java.util.ArrayList;
import java.util.Scanner;

/*
A class that helps display the options for the console
the display*NAME*Actions() methods all offer the same options with small variations
depending on what kind of character commits the action.
 */
public class MenuOptions {

    Scanner sc = new Scanner(System.in);

    public ArmorFactory smithy = new ArmorFactory();

    public WeaponFactory armory = new WeaponFactory();

    public SpellFactory library = new SpellFactory();

    public Hero pickTarget(ArrayList<Hero> party){
        System.out.println("Pick a target:");
        for(int i = 1; i < party.size()+1; i++){
            System.out.println(i+". " + party.get(i-1).getClass().getSimpleName());
        }
        String input = sc.nextLine();
        int heroNr = Integer.parseInt(input);
        if(heroNr-1 < party.size() && heroNr-1 >= 0) {
            return party.get(heroNr-1);
        }else{
            return null;
        }
    }

    public Weapon forgeWeapon(Hero theHero, Rarity theRarity){
        Weapon theWeapon = null;
        switch (theHero.getClass().getSimpleName()){
            case "Rogue":
            case "Warrior":
                System.out.println("Choose weapontype:\n1. Axe\n2. Dagger\n3. Sword");
                String bladedInput = sc.nextLine();
                switch (bladedInput){
                    case "1":
                        theWeapon = armory.getWeapon(WeaponType.Axe, theRarity);
                        break;
                    case "2":
                        theWeapon = armory.getWeapon(WeaponType.Dagger, theRarity);
                        break;
                    case "3":
                        theWeapon = armory.getWeapon(WeaponType.Sword, theRarity);
                        break;
                    default:
                        System.out.println("Wrong input when selecting weaponType");
                        break;
                }
                break;
            case "Mage":
            case "Warlock":
            case "Druid":
            case "Priest":
                System.out.println("Choose weapontype:\n1. Wand\n2. Staff");
                String magicInput = sc.nextLine();
                switch (magicInput){
                    case "1":
                        theWeapon = armory.getWeapon(WeaponType.Wand, theRarity);
                        break;
                    case "2":
                        theWeapon = armory.getWeapon(WeaponType.Staff, theRarity);
                        break;
                    default:
                        System.out.println("Wrong input when selecting weaponType");
                        break;
                }
                break;
            case "Ranger":
                System.out.println("Choose weapontype:\n1. Bow\n2. Crossbow\n3. Gun");
                String rangedInput = sc.nextLine();
                switch (rangedInput){
                    case "1":
                        theWeapon = armory.getWeapon(WeaponType.Bow, theRarity);
                        break;
                    case "2":
                        theWeapon = armory.getWeapon(WeaponType.Crossbow, theRarity);
                        break;
                    case "3":
                        theWeapon = armory.getWeapon(WeaponType.Gun, theRarity);
                        break;
                    default:
                        System.out.println("Wrong input when selecting weaponType");
                        break;
                }
                break;
            case "Paladin":
                System.out.println("Choose weapontype:\n1. Mace\n2. Hammer");
                String bluntInput = sc.nextLine();
                switch (bluntInput){
                    case "1":
                        theWeapon = armory.getWeapon(WeaponType.Mace, theRarity);
                        break;
                    case "2":
                        theWeapon = armory.getWeapon(WeaponType.Hammer, theRarity);
                        break;
                    default:
                        System.out.println("Wrong input when selecting weaponType");
                        break;
                }
                break;
            default:
                System.out.println("Something went wrong. Exiting.");
                break;
        }
        return theWeapon;
    }

    public Armor getArmor(ArmorType theType){
        Armor theArmor = null;
        System.out.println("Choose armor:\n1. Common\n2. Uncommon\n3. Rare\n4. Epic\n5. Legendary");
        String input = sc.nextLine();
        switch (input){
            case "1":
                Common commonRarity = new Common();
                theArmor = smithy.getArmor(theType, commonRarity);
                break;
            case "2":
                Uncommon uncommonRarity = new Uncommon();
                theArmor = smithy.getArmor(theType, uncommonRarity);
                break;
            case "3":
                Rare rareRarity = new Rare();
                theArmor = smithy.getArmor(theType, rareRarity);
                break;
            case "4":
                Epic epicRarity = new Epic();
                theArmor = smithy.getArmor(theType, epicRarity);
                break;
            case "5":
                Legendary legendaryRarity = new Legendary();
                theArmor = smithy.getArmor(theType, legendaryRarity);
                break;
            default:
                System.out.println("You must pick a valid number... returning");
                break;

        }
        return theArmor;
    }

    public Weapon getWeapon(Hero theHero){
        Weapon theWeapon = null;
        System.out.println("Choose Weapon rarity:\n1. Common\n2. Uncommon\n3. Rare\n4. Epic\n5. Legendary");
        String input = sc.nextLine();
        switch (input){
            case "1":
                Common commonRarity = new Common();
                theWeapon = forgeWeapon(theHero, commonRarity);
                break;
            case "2":
                Uncommon uncommonRarity = new Uncommon();
                theWeapon = forgeWeapon(theHero, uncommonRarity);
                break;
            case "3":
                Rare rareRarity = new Rare();
                theWeapon = forgeWeapon(theHero, rareRarity);
                break;
            case "4":
                Epic epicRarity = new Epic();
                theWeapon = forgeWeapon(theHero, epicRarity);
                break;
            case "5":
                Legendary legendaryRarity = new Legendary();
                theWeapon = forgeWeapon(theHero, legendaryRarity);
                break;
            default:
                System.out.println("You must pick a valid number... returning");
                break;

        }
        return theWeapon;
    }

    public Spell getSpell(Hero theHero){
        Spell theSpell = null;
        switch (theHero.getClass().getSimpleName()){
            case "Mage":
            case "Warlock":
                System.out.println("Choose Spell:\n1. ArcaneMissile\n2. ChaosBolt");
                String damageInput = sc.nextLine();
                switch (damageInput){
                    case "1":
                        theSpell = library.getSpell(SpellType.ArcaneMissile);
                        break;
                    case "2":
                        theSpell = library.getSpell(SpellType.ChaosBolt);
                        break;
                    default:
                        System.out.println("Wrong input when selecting spell");
                        break;
                }
                break;
            case "Priest":
                System.out.println("Choose Spell:\n1. Barrier\n2. Rapture");
                String shieldInput = sc.nextLine();
                switch (shieldInput){
                    case "1":
                        theSpell = library.getSpell(SpellType.Barrier);
                        break;
                    case "2":
                        theSpell = library.getSpell(SpellType.Rapture);
                        break;
                    default:
                        System.out.println("Wrong input when selecting spell");
                        break;
                }
                break;
            case "Druid":
                System.out.println("Choose Spell:\n1. Regrowth\n2. SwiftMend");
                String healInput = sc.nextLine();
                switch (healInput){
                    case "1":
                        theSpell = library.getSpell(SpellType.Regrowth);
                        break;
                    case "2":
                        theSpell = library.getSpell(SpellType.SwiftMend);
                        break;
                    default:
                        System.out.println("Wrong input when selecting spell");
                        break;
                }
                break;
            default:
                System.out.println("Something went wrong when the character was going to pick a spell");
                break;
        }

        return theSpell;
    }

    public static void statCheck(Hero theHero){
        double hp = theHero.getCurrentHealth();
        Armor theArmor = theHero.getArmor();
        Weapon theWeapon = theHero.getWeapon();
        System.out.println("Stats: \nHP: "+ hp +"\nWeaponType: " + theWeapon.getWeaponType() +
                "\nWeapon rarity: " + theWeapon.getRarityType() + "\nArmorType: "+ theArmor.getArmorType()+
                "\nArmor rarity: "+ theArmor.getRarityType()+"\nSheild: "+theHero.getShield());

    }

    public void displayRogueActions(Rogue theRogue, ArrayList<Hero> party){
        if(theRogue.getDead()){
            party.remove(theRogue);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Attack\n2. Change weapon\n3. Change Armor\n4. Check stats\n5. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theRogue.attackWithBladedWeapon(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theRogue);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theRogue.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Leather theNewArmor = (Leather) getArmor(ArmorType.Leather);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theRogue.equipArmor(theNewArmor);
                }
                break;
            case "4":
                statCheck(theRogue);
                break;
            default:
                break;
        }
    }

    public void displayDruidActions(Druid theDruid, ArrayList<Hero> party){
        if(theDruid.getDead()){
            party.remove(theDruid);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Heal\n2. Change weapon\n3. Change Armor\n4. " +
                "Change spell\n5. Stat check\n6. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theDruid.healPartyMember(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theDruid);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theDruid.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Leather theNewArmor = (Leather) getArmor(ArmorType.Leather);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theDruid.equipArmor(theNewArmor);
                }
                break;
            case "4":
                System.out.println("Changing Spell");
                Spell theNewSpell = getSpell(theDruid);
                if(theNewSpell == null){
                    System.out.println("Failed to change spell");
                }else{
                    theDruid.changeSpell(theNewSpell);
                }
                break;
            case "5":
                statCheck(theDruid);
                break;
            default:
                break;
        }
    }

    public void displayMageActions(Mage theMage, ArrayList<Hero> party){
        if(theMage.getDead()){
            party.remove(theMage);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Cast spell\n2. Change weapon\n3. Change Armor" +
                "\n4. Change spell\n5. Stat check\n6. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theMage.castDamagingSpell(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theMage);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theMage.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Cloth theNewArmor = (Cloth) getArmor(ArmorType.Cloth);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theMage.equipArmor(theNewArmor);
                }
                break;
            case "4":
                System.out.println("Changing Spell");
                Spell theNewSpell = getSpell(theMage);
                if(theNewSpell == null){
                    System.out.println("Failed to change spell");
                }else{
                    theMage.changeSpell(theNewSpell);
                }
                break;
            case "5":
                statCheck(theMage);
                break;
            default:
                break;
        }
    }

    public void displayRangerActions(Ranger theRanger, ArrayList<Hero> party){
        if(theRanger.getDead()){
            party.remove(theRanger);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Attack\n2. Change weapon\n3. Change Armor\n4. Stat check\n5. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theRanger.attackWithRangedWeapon(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theRanger);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theRanger.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Mail theNewArmor = (Mail) getArmor(ArmorType.Mail);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theRanger.equipArmor(theNewArmor);
                }
                break;
            case "4":
                statCheck(theRanger);
                break;
            default:
                break;
        }
    }

    public void displayWarlockActions(Warlock theWarlcok, ArrayList<Hero> party){
        if(theWarlcok.getDead()){
            party.remove(theWarlcok);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Cast spell\n2. Change weapon\n3. Change Armor" +
                "\n4. Change spell\n5. Stat check\n6. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theWarlcok.castDamagingSpell(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theWarlcok);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theWarlcok.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Cloth theNewArmor = (Cloth) getArmor(ArmorType.Cloth);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theWarlcok.equipArmor(theNewArmor);
                }
                break;
            case "4":
                System.out.println("Changing Spell");
                Spell theNewSpell = getSpell(theWarlcok);
                if(theNewSpell == null){
                    System.out.println("Failed to change spell");
                }else{
                    theWarlcok.changeSpell(theNewSpell);
                }
                break;
            case "5":
                statCheck(theWarlcok);
                break;
            default:
                break;
        }
    }

    public void displayPaladinActions(Paladin thePaladin, ArrayList<Hero> party){
        if(thePaladin.getDead()){
            party.remove(thePaladin);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Attack\n2. Change weapon\n3. Change Armor\n4. Stat check\n5. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    thePaladin.attackWithBluntWeapon(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(thePaladin);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    thePaladin.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Plate theNewArmor = (Plate) getArmor(ArmorType.Plate);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    thePaladin.equipArmor(theNewArmor);
                }
                break;
            case "4":
                statCheck(thePaladin);
                break;
            default:
                break;
        }
    }

    public void displayWarriorActions(Warrior theWarrior, ArrayList<Hero> party){
        if(theWarrior.getDead()){
            party.remove(theWarrior);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Attack\n2. Change weapon\n3. Change Armor\n4. Stat check\n5. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    theWarrior.attackWithBladedWeapon(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(theWarrior);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    theWarrior.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Plate theNewArmor = (Plate) getArmor(ArmorType.Plate);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    theWarrior.equipArmor(theNewArmor);
                }
                break;
            case "4":
                statCheck(theWarrior);
                break;
            default:
                break;
        }
    }

    public void displayPriestActions(Priest thePriest, ArrayList<Hero> party){
        if(thePriest.getDead()){
            party.remove(thePriest);
            System.out.println("Sorry I'm dead, bye.");
            return;
        }
        System.out.println("Pick and action\n1. Shield\n2. Change weapon\n3. Change Armor" +
                "\n4. Change spell\n5. Stat check\n6. Skip");
        String actionNr = sc.nextLine();
        switch (actionNr){
            case "1":
                Hero theTarget = pickTarget(party);
                if(theTarget != null && theTarget.getDead()){
                    System.out.println("Sorry he is already dead.");
                    break;
                }else {
                    thePriest.shieldPartyMember(theTarget);
                    break;
                }
            case "2":
                System.out.println("Changing weapon");
                Weapon theNewWeapon = getWeapon(thePriest);
                if(theNewWeapon == null){
                    System.out.println("Failed to change weapon");
                }else{
                    thePriest.equipWeapon(theNewWeapon);
                }
                break;
            case "3":
                System.out.println("Changing armor");
                Cloth theNewArmor = (Cloth) getArmor(ArmorType.Cloth);
                if(theNewArmor == null){
                    System.out.println("Failed to change armor");
                } else {
                    thePriest.equipArmor(theNewArmor);
                }
                break;
            case "4":
                System.out.println("Changing Spell");
                Spell theNewSpell = getSpell(thePriest);
                if(theNewSpell == null){
                    System.out.println("Failed to change spell");
                }else{
                    thePriest.changeSpell(theNewSpell);
                }
                break;
            case "5":
                statCheck(thePriest);
                break;
            default:
                break;
        }
    }



}
