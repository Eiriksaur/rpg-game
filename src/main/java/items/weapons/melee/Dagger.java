package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BladedWeapons;

public class Dagger extends Weapon implements BladedWeapons {
    public Dagger(Rarity itemRarity){
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
        magicPowerModifier = 1;
    }
}
