package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BluntWeapons;

public class Mace extends Weapon implements BluntWeapons {

    public Mace(Rarity itemRarity){
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.MACE_ATTACK_MOD;
        magicPowerModifier = 1;
    }
}
