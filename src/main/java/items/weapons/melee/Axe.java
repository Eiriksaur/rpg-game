package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BladedWeapons;

public class Axe extends Weapon implements BladedWeapons {

    public Axe(Rarity itemRarity){
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
        magicPowerModifier = 1;
    }

}
