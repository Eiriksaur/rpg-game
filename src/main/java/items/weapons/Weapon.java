package main.java.items.weapons;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public abstract class Weapon {
    protected double attackPowerModifier;
    protected double magicPowerModifier;
    private double rarityModifier;
    private String rarityType;

    public Weapon(Rarity itemRarity){
        this.rarityModifier = itemRarity.powerModifier();
        this.rarityType = itemRarity.getClass().getSimpleName();
    }

    public double getAttackPowerModifier(){
        return attackPowerModifier;
    }

    public double getMagicPowerModifier(){
        return magicPowerModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    public String getRarityType(){
        return this.rarityType;
    }

    public String getWeaponType(){
        return this.getClass().getSimpleName();
    }
}
