package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun extends Weapon implements RangedWeapon {

    public Gun(Rarity itemRarity){
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
        magicPowerModifier = 1;
    }
}
