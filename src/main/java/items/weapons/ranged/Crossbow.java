package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.RangedWeapon;
import org.w3c.dom.ranges.Range;

public class Crossbow extends Weapon implements RangedWeapon {

    public Crossbow(Rarity itemRarity) {
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
        magicPowerModifier = 1;
    }
}
