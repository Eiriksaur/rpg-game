package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow extends Weapon implements RangedWeapon {

    public Bow(Rarity itemRarity){
        super(itemRarity);
        attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
        magicPowerModifier = 1;
    }
}
