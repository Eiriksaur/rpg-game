package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.MagicWeapons;

public class Staff extends Weapon implements MagicWeapons {
    public Staff(Rarity itemRarity){
        super(itemRarity);
        magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
        attackPowerModifier = 1;

    }
}
