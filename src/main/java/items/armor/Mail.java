package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Mail extends Armor{
    // Constructors
    public Mail(Rarity itemRarity) {
        super(itemRarity);
        healthModifier = ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    }
}
