package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Cloth extends Armor{
    // Constructors
    public Cloth(Rarity itemRarity) {
        super(itemRarity);
        healthModifier = ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;

    }

}
