package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Leather extends Armor {
    // Constructors
    public Leather(Rarity itemRarity) {
        super(itemRarity);
        healthModifier = ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    }
}
