package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Plate extends Armor{
    // Constructors
    public Plate(Rarity itemRarity) {
        super(itemRarity);
        healthModifier = ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    }
}
