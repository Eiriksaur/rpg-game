package main.java.items.armor;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public abstract class Armor {
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    private double rarityModifier;
    private String rarityType;

    public Armor(Rarity itemRarity){
        this.rarityModifier = itemRarity.powerModifier();
        this.rarityType = itemRarity.getClass().getSimpleName();
    }

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    public String getRarityType(){
        return this.rarityType;
    }

    public String getArmorType(){
        return this.getClass().getSimpleName();
    }

}
