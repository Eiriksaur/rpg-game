package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Uncommon extends Rarity{
    public Uncommon() {
        this.powerModifier = 1.2;
        this.itemRarityColor = Color.GREEN;
    }
}