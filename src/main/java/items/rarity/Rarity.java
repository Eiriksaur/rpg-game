package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Rarity {
    // Stat modifier
    protected double powerModifier;
    // Color for display purposes
    protected String itemRarityColor;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
