package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Rare extends Rarity{
    public Rare() {
        this.powerModifier = 1.4;
        this.itemRarityColor = Color.BLUE;
    }
}