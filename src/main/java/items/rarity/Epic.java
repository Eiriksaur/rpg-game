package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Epic extends Rarity{
    public Epic(){
        this.powerModifier = 1.6;
        this.itemRarityColor = Color.MAGENTA;
    }
}
