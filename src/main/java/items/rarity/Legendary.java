package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Legendary extends Rarity{
    public Legendary() {
        this.powerModifier = 2;
        this.itemRarityColor = Color.YELLOW;
    }
}