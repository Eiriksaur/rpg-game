package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Common extends Rarity {
    public Common(){
        this.powerModifier = 1;
        this.itemRarityColor = Color.WHITE;
    }
}
