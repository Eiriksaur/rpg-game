package main.java;

import java.util.Scanner;
import main.java.characters.Hero;
import main.java.characters.abstractions.*;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melees.Paladin;
import main.java.characters.melees.Rogue;
import main.java.characters.melees.Warrior;
import main.java.characters.ranged.Ranger;
import main.java.characters.supports.Druid;
import main.java.characters.supports.Priest;
import main.java.consolehelpers.Color;
import main.java.consolehelpers.MenuOptions;
import main.java.factories.*;
import java.util.ArrayList;


public class Main {
    static MenuOptions mo = new MenuOptions();
    static Scanner sc = new Scanner(System.in);

    //Factories for creating characters
    static MeleeFactory meleeFactory = new MeleeFactory();
    static SupportFactory supFactory = new SupportFactory();
    static CasterFactory galtvort = new CasterFactory();
    static RangedFactory woods = new RangedFactory();

    /*The main method runs to primary loops.
    The first for creating and adding characters to the party
    The second to give the user options as to which character he wants to controll
    and what he wants them to do.
     */
    public static void main(String[] args) {
        System.out.println(Color.RED + "RED COLORED" +
                Color.RESET + " NORMAL");
        ArrayList<Hero> party = new ArrayList();


        boolean rogueExists = false;
        boolean druidExists = false;
        boolean mageExists = false;
        boolean rangerExists = false;
        boolean paladinExists = false;
        boolean warriorExists = false;
        boolean warlockExists = false;
        boolean priestExists = false;
        boolean makingChar = true;
        boolean pickingChar = true;

        while(makingChar) {
            System.out.println("Create party members \n1. Rouge\n2. Druid\n3. Mage\n4. Ranger" +
                    "\n5. Warlock\n6. Paladin\n7. Warior\n8. Priest\nx. Finish");
            String charNr = sc.nextLine();
            switch (charNr) {
                case "1":
                    if(rogueExists){
                        System.out.println("The party already has a rogue.");
                    }else {
                        Rogue sneakyMan = (Rogue) meleeFactory.getCharacter(CharacterType.Rogue);
                        System.out.println("Rogue added to the party.");
                        rogueExists = true;
                        party.add(sneakyMan);
                    }
                    break;
                case "2":
                    if(druidExists){
                        System.out.println("The party already has a druid.");
                    }else {
                        Druid beastyBoy = (Druid) supFactory.getCharacter(CharacterType.Druid);
                        party.add(beastyBoy);
                        druidExists = true;
                        System.out.println("Druid added to the party.");
                    }
                    break;
                case "3":
                    if(mageExists){
                        System.out.println("The party already has a Mage.");
                    }else {
                        Mage harryPotter = (Mage) galtvort.getCharacter(CharacterType.Mage);
                        party.add(harryPotter);
                        mageExists = true;
                        System.out.println("Mage added to the party.");
                    }
                    break;
                case "4":
                    if(rangerExists){
                        System.out.println("The party already has a Ranger.");
                    }else {
                        Ranger aragorn = (Ranger) woods.getCharacter(CharacterType.Ranger);
                        party.add(aragorn);
                        rangerExists = true;
                        System.out.println("Ranger added to the party.");
                    }
                    break;
                case "5":
                    if(warlockExists){
                        System.out.println("The party already has a Warlock.");
                    }else {
                        Warlock ghorlo = (Warlock) galtvort.getCharacter(CharacterType.Warlock);
                        party.add(ghorlo);
                        warlockExists = true;
                        System.out.println("Warlock added to the party.");
                    }
                    break;
                case "6":
                    if(paladinExists){
                        System.out.println("The party already has a Paladin.");
                    }else {
                        Paladin palle = (Paladin) meleeFactory.getCharacter(CharacterType.Paladin);
                        party.add(palle);
                        paladinExists = true;
                        System.out.println("Paladin added to the party.");
                    }
                    break;
                case "7":
                    if(warriorExists){
                        System.out.println("The party already has a Warrior.");
                    }else {
                        Warrior conan = (Warrior) meleeFactory.getCharacter(CharacterType.Warrior);
                        party.add(conan);
                        warriorExists = true;
                        System.out.println("Warrior added to the party.");
                    }
                    break;
                case "8":
                    if(priestExists){
                        System.out.println("The party already has a Priest.");
                    }else {
                        Priest peter = (Priest) supFactory.getCharacter(CharacterType.Priest);
                        party.add(peter);
                        priestExists = true;
                        System.out.println("Priest added to the party.");
                    }
                    break;
                case "x":
                    makingChar = false;
                    break;
                default:
                    System.out.println("Wrong input");
                    break;
            }
        }

        if(party.isEmpty()) System.exit(0); //Exits the program if the party is empty;

        //Printing the party to the console1
        System.out.println("The party: ");
        for (Hero member : party) {
            System.out.println(member.getClass().getSimpleName());
        }

        while(pickingChar){
            System.out.println("Pick a party member to control.");
            for(int i = 1; i < party.size()+1; i++){
                System.out.println(i+". " + party.get(i-1).getClass().getSimpleName());
            }
            System.out.println("Any other key will exit.");
            String charNr = sc.nextLine();
            int idxNr;
            try {
                idxNr = Integer.parseInt(charNr);
            } catch (NumberFormatException e){
                pickingChar = false;
                continue;
            }
            switch (party.get(idxNr-1).getClass().getSimpleName()){
                case "Rogue":
                    mo.displayRogueActions((Rogue)party.get(idxNr-1), party);
                    break;
                case "Druid":
                    mo.displayDruidActions((Druid)party.get(idxNr-1), party);
                    break;
                case "Mage":
                    mo.displayMageActions((Mage)party.get(idxNr-1), party);
                    break;
                case "Ranger":
                    mo.displayRangerActions((Ranger)party.get(idxNr-1), party);
                    break;
                case "Warlock":
                    mo.displayWarlockActions((Warlock)party.get(idxNr-1), party);
                    break;
                case "Paladin":
                    mo.displayPaladinActions((Paladin)party.get(idxNr-1), party);
                    break;
                case "Warrior":
                    mo.displayWarriorActions((Warrior) party.get(idxNr-1), party);
                    break;
                case "Priest":
                    mo.displayPriestActions((Priest)party.get(idxNr-1), party);
                    break;
                default:
                    System.out.println("Number out of range.");
            }
        }
    }
}
