package main.java.characters.ranged;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Mail;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.ranged.Bow;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Hero implements Ranged {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;

    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Constructor
    public Ranger() {
        // When the character is created it has maximum health (base health)
        baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED; // Armor
        baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        Mail theArmor = new Mail(starterRarity);
        this.armor =  theArmor;
        Bow theWeapon = new Bow(starterRarity);
        this.weapon = theWeapon;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    // Character behaviours
    public void equipArmor(Armor armor) {
        if(armor instanceof Mail){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof RangedWeapon){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    /**
     * Damages the enemy
     */
    @Override
    public double attackWithRangedWeapon(Hero target) {
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        } else {
            double attackPower = baseAttackPower*weapon.getAttackPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Ranger I got a shooty shooty! Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "physical");
            return attackPower;
        }
    }
}
