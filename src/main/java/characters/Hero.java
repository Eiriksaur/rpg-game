package main.java.characters;

import main.java.items.armor.Armor;
import main.java.items.rarity.Common;
import main.java.items.weapons.Weapon;

//A super class for all the hero characters in the game
public abstract class Hero {
    private double shield = 0;
    protected double currentHealth;
    protected boolean isDead = false;

    protected double baseHealth;
    protected double basePhysReductionPercent;
    protected double baseMagicReductionPercent;

    protected Common starterRarity = new Common();
    protected Armor armor;
    protected Weapon weapon;

    //Getters and Setters
    public double getShield(){
        return this.shield;
    }

    public void setShield(double newShield){
        this.shield = newShield;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double currentHealth) {
        this.currentHealth = currentHealth;
    }

    public double getCurrentMaxHealth() { return baseHealth * this.armor.getHealthModifier();}

    public boolean getDead() {
        return isDead;
    }

    public Armor getArmor(){
        return this.armor;
    }

    public Weapon getWeapon(){
        return this.weapon;
    }

    //Behaviours
    public abstract void equipArmor(Armor armor);

    public abstract void equipWeapon(Weapon weapon);

    public double damageShield(double incomingDamage){
        if(incomingDamage > this.shield){
            incomingDamage -= this.shield;
            setShield(0);
            return incomingDamage;
        } else {
            double newShieldValue = this.shield - incomingDamage;
            setShield(newShieldValue);
            return 0;
        }
    }

    public double recieveHealing(double incomingHealing){
        setCurrentHealth(getCurrentHealth()+incomingHealing);
        System.out.println("I'm a " + this.getClass().getSimpleName() + "! That felt refreshing.");
        return incomingHealing;
    }

    public double getShielded(double incomingShield){
        double newShieldValue = this.shield + incomingShield;
        setShield(newShieldValue);
        System.out.println("I'm a " + this.getClass().getSimpleName() + " and now I have a shield.");
        return incomingShield;
    }

    public double takeDamage(double incomingDamage, String damageType) {
        double damageTaken = 0;
        String respons = "I'm a " + this.getClass().getSimpleName() + "! OUCH! That hurts... ";
        switch(damageType){
            case "magic":
                damageTaken = damageShield(incomingDamage * (1 -
                        baseMagicReductionPercent * this.armor.getMagicRedModifier() *
                                this.armor.getRarityModifier()));
                break;
            case "physical":
                damageTaken = damageShield(incomingDamage * (1 -
                        basePhysReductionPercent * this.armor.getPhysRedModifier() *
                                this.armor.getRarityModifier()));
                break;
            default:
                System.out.println("The damage type is not supported");

        }
        setCurrentHealth(this.currentHealth-damageTaken);
        respons += "-" + damageTaken + "HP";
        if(currentHealth <= 0){
            isDead = true;
            respons = "I'm dead.... that sucks";
        }
        System.out.println(respons);
        return damageTaken;
    }
}
