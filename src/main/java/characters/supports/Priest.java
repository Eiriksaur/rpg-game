package main.java.characters.supports;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Support;
import main.java.items.armor.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.MagicWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.Spell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Hero implements Support {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private ShieldingSpell shieldingSpell;

    public Priest(ShieldingSpell shieldingSpell) {
        this.baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        this.shieldingSpell = shieldingSpell;
        Cloth theArmor = new Cloth(starterRarity);
        this.armor =  theArmor;
        Wand theWeapon = new Wand(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Cloth){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof MagicWeapons){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    public void changeSpell(Spell theSpell){
        if(theSpell instanceof ShieldingSpell){
            this.shieldingSpell = (ShieldingSpell)theSpell;
        }else{
            System.out.println("Cant use this spell");
        }
    }

    /**
     * Shields a party member for a percentage of their maximum health.
     */
    public double shieldPartyMember(Hero target) {
        if(target == null){
            System.out.println("Shield Missed");
            return 0;
        } else {
            double current = target.getCurrentHealth();
            double maxHealth = target.getCurrentMaxHealth();
            double shieldingammount = shieldingSpell.getAbsorbShieldPercentage() *
                    target.getCurrentMaxHealth()*weapon.getMagicPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Priest I deploy them sheilds! Sheilding for: " + shieldingammount + "HP");
            target.getShielded(shieldingammount);
            return shieldingammount;
        }
    }

}
