package main.java.characters.supports;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Support;
import main.java.items.armor.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.MagicWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.Spell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Hero implements Support {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private HealingSpell healingSpell;

    public Druid(HealingSpell healingSpell) {
        this.baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        this.healingSpell = healingSpell;
        Leather theArmor = new Leather(starterRarity);
        this.armor =  theArmor;
        Wand theWeapon = new Wand(starterRarity);
        this.weapon = theWeapon;
    }

    //Character behaviour
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Leather){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof MagicWeapons){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    public void changeSpell(Spell theSpell){
        if(theSpell instanceof HealingSpell){
            this.healingSpell = (HealingSpell)theSpell;
        }else{
            System.out.println("Cant use this spell");
        }
    }

    /**
     * Heals the party member
     */
    public double healPartyMember(Hero target) {
        if(target == null){
            System.out.println("Healing Missed");
            return 0;
        } else {
            double current = target.getCurrentHealth();
            double maxHealth = target.getCurrentMaxHealth();
            double healingammount = healingSpell.getHealingAmount()* weapon.getMagicPowerModifier()*
                    weapon.getRarityModifier();
            if(current + healingSpell.getHealingAmount() > maxHealth){
                /*It is not possible to heal someone for an
                amount that would push them above their max health
                 */
                healingammount = maxHealth - current;
            }
            System.out.println("I'm a Druid I keep them boys healthy! Healing for: " + healingammount + "HP");
            target.recieveHealing(healingammount);
            return healingammount;
        }

    }
}
