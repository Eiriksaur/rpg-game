package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BladedWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Sword;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Hero implements Melee {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;

    // Constructor
    public Warrior() {
        // When the character is created it has maximum health (base health)
        this.baseHealth = CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        Plate theArmor = new Plate(starterRarity);
        this.armor = theArmor;
        Sword theWeapon = new Sword(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Plate){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof BladedWeapons){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon(Hero target){
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        } else {
            double attackPower = baseAttackPower*weapon.getAttackPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Warrior I go berserk! Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "physical");
            return attackPower;
        }
    }

}
