package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BluntWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Mace;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Hero implements Melee {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BluntWeapon;

    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;

    // Constructor
    public Paladin() {
        // When the character is created it has maximum health (base health)
        this.baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        Plate theArmor = new Plate(starterRarity);
        this.armor =  theArmor;
        Mace theWeapon = new Mace(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Plate){
            this.armor = armor;
        }else{
            System.out.println("Cant use that armor");
        }

    }
    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof BluntWeapons){
            this.weapon = weapon;
        } else{
            System.out.println("Cant use that weapon");
        }
    }

    public double attackWithBluntWeapon(Hero target) {
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        } else {
            double attackPower = baseAttackPower * weapon.getAttackPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Paladin I hit hard! Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "physical");
            return attackPower;
        }
    }

}
