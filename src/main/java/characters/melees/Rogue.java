package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.BladedWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Dagger;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Hero implements Melee {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Constructor
    public Rogue() {
        // When the character is created it has maximum health (base health)
        this.baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        Leather theArmor = new Leather(starterRarity);
        this.armor =  theArmor;
        Dagger theWeapon = new Dagger(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Leather){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof BladedWeapons){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon(Hero target) {
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        }else {
            double attackPower = baseAttackPower*weapon.getAttackPowerModifier()*weapon.getRarityModifier();
            System.out.println("I am a rogue I go 'Stabby stabby!' Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "physical");
            return attackPower;
        }
    }

}
