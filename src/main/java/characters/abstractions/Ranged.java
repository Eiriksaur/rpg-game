package main.java.characters.abstractions;

import main.java.characters.Hero;

public interface Ranged {
    public double attackWithRangedWeapon(Hero target);
}
