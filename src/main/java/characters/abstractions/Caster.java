package main.java.characters.abstractions;

import main.java.characters.Hero;

public interface Caster {
    public abstract double castDamagingSpell(Hero target);
}
