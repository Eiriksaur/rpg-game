package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.MagicWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.Spell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Hero implements Caster {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private DamagingSpell damagingSpell;

    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    public Mage(DamagingSpell damagingSpell) {
        this.baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        this.damagingSpell = damagingSpell;
        Cloth theArmor = new Cloth(starterRarity);
        this.armor =  theArmor;
        Staff theWeapon = new Staff(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Cloth){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof MagicWeapons){
            this.weapon=weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    public void changeSpell(Spell theSpell){
        if(theSpell instanceof DamagingSpell){
            this.damagingSpell = (DamagingSpell)theSpell;
        }else{
            System.out.println("Cant use this spell");
        }
    }

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell(Hero target) {
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        } else {
            double attackPower = damagingSpell.getSpellDamageModifier()*
                    baseMagicPower*weapon.getAttackPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Mage I throw them spells! Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "magic");
            return attackPower;
        }
    }

}
