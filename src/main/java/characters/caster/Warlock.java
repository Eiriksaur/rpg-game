package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.Weapon;
import main.java.items.weapons.abstractions.MagicWeapons;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.Spell;
/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Hero implements Caster {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private DamagingSpell damagingSpell;

    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;


    public Warlock(DamagingSpell damagingSpell) {
        this.baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
        this.basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED; // Armor
        this.baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES; // Magic armor
        this.currentHealth = baseHealth;
        this.damagingSpell = damagingSpell;
        Cloth theArmor = new Cloth(starterRarity);
        this.armor =  theArmor;
        Staff theWeapon = new Staff(starterRarity);
        this.weapon = theWeapon;
    }

    // Character behaviours
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof Cloth){
            this.armor = armor;
        }else{
            System.out.println("Cant use this armor");
        }
    }

    @Override
    public void equipWeapon(Weapon weapon) {
        if(weapon instanceof MagicWeapons){
            this.weapon = weapon;
        }else{
            System.out.println("Cant use this weapon");
        }
    }

    public void changeSpell(Spell theSpell){
        if(theSpell instanceof DamagingSpell){
            this.damagingSpell = (DamagingSpell)theSpell;
        }else{
            System.out.println("Cant use this spell");
        }
    }

    /**
     * Damages the enemy with its spells
     */
    @Override
    public double castDamagingSpell(Hero target) {
        if(target == null){
            System.out.println("Attack Missed");
            return 0;
        } else {
            double attackPower = damagingSpell.getSpellDamageModifier()*
                    baseMagicPower*weapon.getMagicPowerModifier()*weapon.getRarityModifier();
            System.out.println("I'm a Warlock I throw them spells! Attack could deal up to " + attackPower + "HP");
            target.takeDamage(attackPower, "magic");
            return attackPower;
        }
    }

}
