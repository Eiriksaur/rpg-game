package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    SwiftMend,
    Rapture,
    Regrowth
}
