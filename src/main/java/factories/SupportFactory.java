package main.java.factories;
// Imports
import main.java.characters.supports.Druid;
import main.java.characters.supports.Priest;
import main.java.characters.abstractions.Support;
import main.java.characters.abstractions.CharacterType;
import main.java.spells.healing.Regrowth;
import main.java.spells.shielding.Barrier;

public class SupportFactory {
    Regrowth smallHeal = new Regrowth();
    Barrier smallSheild = new Barrier();
    public Support getCharacter(CharacterType charType) {
        switch(charType) {
            case Druid:
                return new Druid(smallHeal);
            case Priest:
                return new Priest(smallSheild);
            default:
                return null;
        }
    }
}
