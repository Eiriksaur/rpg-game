package main.java.factories;
// Imports
import main.java.characters.supports.Druid;
import main.java.characters.Hero;
import main.java.characters.melees.Rogue;
import main.java.characters.abstractions.CharacterType;
import main.java.spells.abstractions.HealingSpell;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public Hero getCharacter(CharacterType charType) {
        switch(charType) {
            case Druid:
                HealingSpell helthyboys = new HealingSpell() {
                    @Override
                    public double getHealingAmount() {
                        return 40;
                    }

                    @Override
                    public String getSpellName() {
                        return "HeltyHehty";
                    }
                };
                return new Druid(helthyboys);
            case Rogue:
                return new Rogue();
            default:
                return null;
        }
    }
}
