package main.java.factories;

import main.java.characters.abstractions.Ranged;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.ranged.Ranger;


public class RangedFactory {
    public Ranged getCharacter(CharacterType charType) {
        switch(charType) {
            case Ranger:
                return new Ranger();
            default:
                return null;
        }
    }
}