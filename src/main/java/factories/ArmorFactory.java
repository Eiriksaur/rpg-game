package main.java.factories;
// Imports
import main.java.items.armor.*;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {
    public Armor getArmor(ArmorType armorType, Rarity itemRarityModifier){
        switch(armorType) {
            case Cloth:
                return new Cloth(itemRarityModifier);
            case Leather:
                return new Leather(itemRarityModifier);
            case Mail:
                return new Mail(itemRarityModifier);
            case Plate:
                return new Plate(itemRarityModifier);
            default:
                return null;
        }
    }
}
