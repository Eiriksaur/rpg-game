package main.java.factories;
// Imports

import main.java.characters.abstractions.Melee;
import main.java.characters.melees.Paladin;
import main.java.characters.melees.Rogue;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.melees.Warrior;


public class MeleeFactory {
    public Melee getCharacter(CharacterType charType) {
        switch(charType) {
            case Rogue:
                return new Rogue();
            case Paladin:
                return new Paladin();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
