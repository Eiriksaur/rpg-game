package main.java.factories;
// Imports


import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;


public class CasterFactory {
    ArcaneMissile spicyLightning = new ArcaneMissile();
    ChaosBolt chaosBolt = new ChaosBolt();
    public Caster getCharacter(CharacterType charType) {
        switch(charType) {
            case Mage:
                return new Mage(spicyLightning);
            case Warlock:
                return new Warlock(chaosBolt);
            default:
                return null;
        }
    }
}
